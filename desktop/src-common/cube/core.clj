(ns cube.core
  (:require [play-clj.core :refer :all]
            [play-clj.g3d :refer :all]
            [play-clj.math :refer :all]
            [cube.world :refer :all]
            [cube.graphics :refer :all]))

(defn material-builder [color opacity]
  (let [diffuse-light-attr (attribute! :color :create-diffuse color)
        blending-attr (attribute :blending (gl :gl-src-alpha) (gl :gl-one-minus-src-alpha) opacity)]
    (material :set diffuse-light-attr blending-attr)))
(defn material-white [] (material-builder (color :white) 0.125))

(def game-world (atom world-template))

(defscreen main-screen
  :on-show
  (fn [screen entities]
    (add-timer! screen :game-tick 0 0.2)
    (update! screen
             :renderer (model-batch)
             :attributes (let [ambient-light-type (attribute-type :color :ambient-light)
                               ambient-light-attr (attribute :color ambient-light-type 1.0 1.0 1.0 1)]
                           (environment :set ambient-light-attr))
             :camera (doto (perspective 60 (game :width) (game :height))
                       (position! 1.75 1.75 7)))
    (let [builder (model-builder)
          material (material-white)
          vertex-attr (bit-or (usage :position) (usage :normal))
          size (range 0 4)]
      (for [x size]
        (for [y size]
          (for [z size]
            (-> (model-builder! builder :create-box 0.5 0.5 0.5 material vertex-attr)
                model
                (assoc :x x :y y :z z)))))))

  :on-timer
  (fn [screen entities]
    (if (= (:id screen) :game-tick)
      (swap! game-world update-world))
    entities)

  :on-resize
  (fn [screen entities]
    (height! screen 1024))
  
  :on-render
  (fn [screen entities]
    (clear!)
    (doto screen
      (perspective! :rotate-around (vector-3 1.75 1.75 1.75) (vector-3 0 1 0) 0.25)
      (perspective! :rotate-around (vector-3 1.75 1.75 1.75) (vector-3 1 0 0) 0.125)
      (perspective! :rotate-around (vector-3 1.75 1.75 1.75) (vector-3 0 0 1) 0.0625)
      (perspective! :update))

    (doseq [model entities]
      (material! (first (model! model :materials)) :set (material-white)))

    (let [{snake :snake dot :dot ticks :ticks} (deref game-world)]
      (if (not (nil? dot))
        (let [model (first (filter #(and (= (:x dot) (:x %)) (= (:y dot) (:y %)) (= (:z dot) (:z %))) entities))
              ripeness (:ripeness dot)
              blink-rate (if (pos? ripeness) 16 8)
              hsv (hsv (* (mod ripeness 360) blink-rate) (min 1.0 (/ (+ ripeness 100) 100.0)) 1)
              c (color (/ (:red hsv) 255) (/ (:green hsv) 255) (/ (:blue hsv) 255) 1)]
          (material! (first (model! model :materials)) :set (material-builder c 0.75))))
      (doseq [{x :x y :y z :z} (snake :nodes)]
        (let [model (first (filter #(and (= x (:x %)) (= y (:y %)) (= z (:z %))) entities))
              hsv (hsv (* (mod ticks 360) 4) 1 1)
              c (color (/ (:red hsv) 255) (/ (:green hsv) 255) (/ (:blue hsv) 255) 1)]
          (material! (first (model! model :materials)) :set (material-builder c 0.75))))
      (render! screen entities))))

(defgame cube-game
  :on-create
  (fn [this]
    (set-screen! this main-screen)))
