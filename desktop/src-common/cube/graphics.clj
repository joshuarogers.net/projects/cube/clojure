(ns cube.graphics)

(defn hsv [hue saturation value]
  (let [h1 (/ hue 60)
        f (mod h1 1)
        v1 (* value 255)
        pv (* (- 1 saturation) v1)
        qv (* (- 1 (* saturation f)) v1)
        tv (* (- 1 (* saturation (- 1 f))) v1)
        opt (mod (int h1) 6)]
    (case opt
      0 { :red v1 :green tv :blue pv }
      1 { :red qv :green v1 :blue pv }
      2 { :red pv :green v1 :blue tv }
      3 { :red pv :green qv :blue v1 }
      4 { :red tv :green pv :blue v1 }
      5 { :red v1 :green pv :blue qv })))
