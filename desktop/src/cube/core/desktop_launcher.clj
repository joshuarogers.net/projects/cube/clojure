(ns cube.core.desktop-launcher
  (:require [cube.core :refer :all])
  (:import [com.badlogic.gdx.backends.lwjgl LwjglApplication]
           [org.lwjgl.input Keyboard])
  (:gen-class))

(defn -main
  []
  (LwjglApplication. cube-game "cube" 800 600)
  (Keyboard/enableRepeatEvents true))
